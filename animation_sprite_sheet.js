let heroSprite = 'theflash';
let logoHero = 'logoFlash';

// menu
let menu;
let flashMenu;
let batmanMenu;
let spriteFlashMenu;
let spriteBatmanMenu;

// image to load
let heroBottom;
let heroTop;
let heroRight;
let heroLeft;
let logoHeroBarImg;
let injuredImg;
let batarangImg;
let logoSpymanImg;

// sprites
let speedBarSprite;
let hero;
let bg;
let injuredSprite;
let nbreBatarangSprite;
let logoSpymanSprite;
let spriteHowToPlay;

// groups
let speedBarsGroup;
let boostGroup;
let injuredGroup;
let batarangsGroup;
let nbreBatarangsGroups;
let logoGroup;

// scene variable
let SCENE_W = 800;
let SCENE_H = 800;

// variable hero
let heroSpeed = 8;
let heroVelocityBase = 0.15;
let heroVelocityMax = 8;
let speedBar = 100;
let growSpeed = false;
let isHeroFlash = true;
let rotationBatarang = 90;
let nbreOfBatarang = 90;

// external variables
let timer = 0;
let fullTime = 90;
let timerLogo = 2;
let injured = 20;
let boost = 5;
let gameStart = false;
let gameIsOver = false;
let showLogoSpyman = true;
let isHowTo = false;
let nbrePizzas = 50;

// font
let fontGame;

function preload() {
  fontGame = loadFont('assets/BATMAN.ttf');
  textFont(fontGame);
  loadLogo();
  loadHero();
  loadVariables();
  loadMenu();
}


function setup() {
  createCanvas(1046,876);
  if (showLogoSpyman) {
    chrono();
  }
}

function draw() {
  if( !gameStart && !gameIsOver && !showLogoSpyman && !isHowTo) {
    choosePlayer();
  } else if (!gameStart && gameIsOver && !showLogoSpyman && !isHowTo){
    endGamePage();
  } else if(gameStart && !gameIsOver && !showLogoSpyman && !isHowTo){
    playGame();
  } else if (!gameStart && !gameIsOver && showLogoSpyman && !isHowTo) {
    showLogoSpy();
  } else if (!gameStart && !gameIsOver && !showLogoSpyman && isHowTo) {
    drawHowToPlay();
  }

}

function loadHero() {
  let hero_left = [
    {"name":"hero_left01", "frame":{"x":0, "y": 64, "width": 64, "height": 64}},
    {"name":"hero_left02", "frame":{"x":64, "y": 64, "width": 64, "height": 64}},
    {"name":"hero_left03", "frame":{"x":128, "y": 64, "width": 64, "height": 64}},
    {"name":"hero_left04", "frame":{"x":192, "y": 64, "width": 64, "height": 64}},
  ];

  let hero_right = [
    {"name":"hero_right01", "frame":{"x":0, "y": 128, "width": 64, "height": 64}},
    {"name":"hero_right02", "frame":{"x":64, "y": 128, "width": 64, "height": 64}},
    {"name":"hero_right03", "frame":{"x":128, "y": 128, "width": 64, "height": 64}},
    {"name":"hero_right04", "frame":{"x":192, "y": 128, "width": 64, "height": 64}},
  ];

  let hero_top = [
    {"name":"hero_left01", "frame":{"x":0, "y": 192, "width": 64, "height": 64}},
    {"name":"hero_left02", "frame":{"x":64, "y": 192, "width": 64, "height": 64}},
    {"name":"hero_left03", "frame":{"x":128, "y": 192, "width": 64, "height": 64}},
    {"name":"hero_left04", "frame":{"x":192, "y": 192, "width": 64, "height": 64}},
  ];


  heroBottom = loadSpriteSheet('assets/'+heroSprite+'.png', 64, 64, 4);
  heroLeft = loadSpriteSheet('assets/'+heroSprite+'.png', hero_left);
  heroRight = loadSpriteSheet('assets/'+heroSprite+'.png', hero_right);
  heroTop = loadSpriteSheet('assets/'+heroSprite+'.png', hero_top);
  hero = createSprite(60, 70, 50, 100);
  hero.addAnimation('goDown', heroBottom);
  hero.addAnimation('goUp', heroTop);
  hero.addAnimation('goLeft', heroLeft);
  hero.addAnimation('goRight', heroRight);

  batarangImg = loadSpriteSheet('assets/batarang.png', 21, 21, 5);
}

function loadVariables () {
  bg = new Group();
  batarangsGroup = new Group();
  logoHeroBarImg = loadImage('assets/'+logoHero+'.png');
  speedBarsGroup = new Group();
  // flash
  let xSpeedBarSprite = 850;
  for(let i = 0; i < 3; i++) {
    speedBarSprite = createSprite(xSpeedBarSprite , 820);
    xSpeedBarSprite += 65;
    speedBarSprite.addImage(logoHeroBarImg);
    speedBarsGroup.add(speedBarSprite);
  }
  // batman
  nbreBatarangsGroups = new Group();
  nbreBatarangSprite = createSprite(900, 820);
  nbreBatarangSprite.addImage(logoHeroBarImg);
  nbreBatarangsGroups.add(nbreBatarangSprite);
  loadTiles();
  loadInjuredPeople();
}

// load tiles
function loadTiles() {
  // load images
  let roadHorizontal = loadImage('assets/tiles/roadHorizontal.png');
  let roadVertical = loadImage('assets/tiles/roadVertical.png');
  let pavementUp = loadImage('assets/tiles/pavementUp.png');
  let pavementDown = loadImage('assets/tiles/pavementDown.png');
  let pavementLeft = loadImage('assets/tiles/pavementLeft.png');
  let pavementRight = loadImage('assets/tiles/pavementRight.png');
  let pavementLeftBis = loadImage('assets/tiles/pavementLeftBis.png');
  let pavementRightBis = loadImage('assets/tiles/pavementRightBis.png');

  let array = new Array()
  let name = '';

  for(let i = -SCENE_W; i <= SCENE_W; i+=100) {
    array[i + SCENE_W] = new Array();
    for(let j = -SCENE_H; j <= SCENE_H; j+=100) {
      let tile = createSprite(i, j);
      if (i === -SCENE_W) {
        if (random(0, 1) > 0.8) {
          name ='pavementLeftBis';
          tile.addImage(pavementLeftBis)
        } else {
          name ='pavementLeft';
          tile.addImage(pavementLeft);
        }
      } else if (i === SCENE_W) {
        if (random(0, 1) > 0.8) {
          name ='pavementRightBis';
          tile.addImage(pavementRightBis)
        } else {
          name ='pavementRight';
          tile.addImage(pavementRight);
        }
      } else if (j === -SCENE_H && i !== -SCENE_W + 100 && i !== SCENE_W - 100) {
        name ='pavementUp';
        tile.addImage(pavementUp);
      } else if(j === SCENE_H && i !== -SCENE_W + 100 && i !== SCENE_W - 100) {
        name ='pavementDown';
        tile.addImage(pavementDown);
      } else if (i === -SCENE_W + 100) {
        name = 'roadVertical';
        tile.addImage(roadVertical);
      } else if (i === SCENE_W - 100) {
        name = 'roadVertical';
        tile.addImage(roadVertical);
      } else if (i >= -SCENE_W + 200 && i <= SCENE_W - 200 && j % 300 === 0 && j > -SCENE_H + 200 && j < SCENE_H - 100) {
        name ='pavementDown';
        tile.addImage(pavementDown);
      } else if (i >= -SCENE_W + 200 && i <= SCENE_W - 200 && (j-100) % 300 === 0 && j > -SCENE_H + 200 && j < SCENE_H - 100) {
        name ='pavementUp';
        tile.addImage(pavementUp);
      } else {
        name = 'roadHorizontal';
        tile.addImage(roadHorizontal);
      }
      array[i + SCENE_W][j + SCENE_H] = name;
      bg.add(tile);
    }
  }
}

function loadInjuredPeople() {
  injuredGroup = new Group();
  injuredImg = loadSpriteSheet('assets/pizza.png', 50, 50, 8);
  for(let i=0; i<nbrePizzas; i++) {
    let rescue = createSprite(random(-SCENE_W + 100, SCENE_W -  100), random(-SCENE_H + 100, SCENE_H - 100));
    rescue.addAnimation('normal', injuredImg);
    injuredGroup.add(rescue);
  }
}


function loadMenu() {
  menu = new Group();
  flashMenu = loadImage('assets/menu/flash.png');
  batmanMenu = loadImage('assets/menu/batman.png');

  spriteFlashMenu = createSprite(250, 300);
  spriteBatmanMenu = createSprite(775, 300);
  spriteHowToPlay = createSprite(525, 750, 200, 40);

  spriteFlashMenu.addImage(flashMenu);
  spriteBatmanMenu.addImage(batmanMenu);

  spriteFlashMenu.mouseActive = true;
  spriteBatmanMenu.mouseActive = true;
  spriteHowToPlay.mouseActive = true;


  menu.add(spriteFlashMenu);
  menu.add(spriteBatmanMenu);
}

function loadLogo() {
  logoGroup = new Group();
  logoSpymanImg = loadImage('assets/logoSpyman.png');
}

function showLogoSpy() {
  background(255);
  logoSpymanSprite = createSprite(width/2, height/2);
	logoSpymanSprite.addImage(logoSpymanImg);
  logoGroup.add(logoSpymanSprite);
  drawSprites(logoGroup);
}

function choosePlayer() {
  camera.off();
  background(0);

  drawSprites(menu);

  fill(250, 230, 0);
  textSize(20);
  text('Choose your player !', 425, 700);
  text('How to play ?', 460, 750);
  textSize(15);
  text('Use your mouse to select your hero or go to how to play :)', 300, 850);
  spriteHowToPlay.visible = false;
  if (spriteHowToPlay.mouseIsPressed) {
    isHowTo = true;
    clear();
  }
  drawSprite(spriteHowToPlay);


  if(menu[0].mouseIsPressed) {
    heroSprite = 'theflash';
    logoHero = 'logoFlash';
    heroSpeed = 8;
    isHeroFlash = true;
    heroVelocityBase = 0.15;
    heroVelocityMax = 8;
    startGame();
  }
  if(menu[1].mouseIsPressed) {
    heroSprite = 'thebatman4';
    logoHero = 'logoBatman';
    heroSpeed = 6;
    isHeroFlash = false;
    heroVelocityBase = 0.25;
    heroVelocityMax = 12;
    startGame();
  }
}

function drawHowToPlay() {
  camera.off();
  background(0);
  fill(250, 230, 0);
  textSize(20);
  text('You have to choose your hero, each hero has a power!', 250, 250);
  text('You need to use arrow keys to make your movement', 250, 300);
  text('As Flash, maintain CTRL to have your speed at max', 250, 350);
  text('As Batman, click on CTRL use your batarangs', 250, 400);
  text('You can quit the game with Echap', 250, 450);
  text('Be careful, you  have limited batarang or speed!', 250, 500);
  text('Click on your mouse or X to view a part of the map!', 250, 550);
  text('Enjoy :)', 250, 600);

  text('Echap to go back to the menu!', 250, 700);

  if(keyWentDown(27)) {
    isHowTo =false;
    clear();
  }
}

function startGame() {
  loadHero();
  loadVariables();
  gameStart = true;
  fullTime = 90;
  nbreOfBatarang = 90;
	chrono();
  clear();
}


function playGame() {
  background(0);

    if(keyWentDown(27)) {
      gameStart =false;
      clear();
    }

    //.5 zoom is zooming out (50% of the normal size)
    if(mouseIsPressed || keyIsDown(88))
      camera.zoom = .5;
    else
      camera.zoom = 1;


    if (keyIsDown(LEFT_ARROW) && hero.velocity.y === 0) {
      hero.changeAnimation('goLeft');
      if (hero.velocity.x > -heroVelocityMax)
        hero.velocity.x -= heroVelocityBase * heroSpeed;
      else
        hero.velocity.x = -heroVelocityMax;
      rotationBatarang = 180;
      hero.animation.play();
    }
    else if (keyIsDown(RIGHT_ARROW) && hero.velocity.y === 0){
      hero.changeAnimation('goRight');
      if (hero.velocity.x < heroVelocityMax)
        hero.velocity.x += heroVelocityBase * heroSpeed;
      else
        hero.velocity.x = heroVelocityMax;

      rotationBatarang = 0;
      hero.animation.play();

    }
    else if (keyIsDown(UP_ARROW) && hero.velocity.x === 0) {
      hero.changeAnimation('goUp');
      if (hero.velocity.y > -heroVelocityMax)
        hero.velocity.y -= heroVelocityBase * heroSpeed;
      else
        hero.velocity.y = -heroVelocityMax;
      rotationBatarang = -90;
      hero.animation.play();
    }
    else if (keyIsDown(DOWN_ARROW) && hero.velocity.x === 0) {
      hero.changeAnimation('goDown');
      if (hero.velocity.y < heroVelocityMax)
        hero.velocity.y += heroVelocityBase * heroSpeed;
      else
        hero.velocity.y = heroVelocityMax;

      rotationBatarang = 90;
      hero.animation.play();
    }
    else {
      hero.velocity.x = 0;
      hero.velocity.y = 0;

      hero.animation.stop();
    }
    if(isHeroFlash) {
      if (keyIsDown(CONTROL) && heroSpeed > 1 && speedBarsGroup.length > 0) {
        if( frameCount%100 === 0 && speedBar > 0) {
          heroSpeed /= 2;
          heroVelocityMax /= (1/heroSpeed);
          speedBar -= 5;
          speedBarsGroup.remove(speedBarsGroup.get(speedBarsGroup.length - 1));
        }
      } else {
        if( frameCount%40 === 0) {
          if (heroSpeed < 8) {
            heroSpeed *= 2;
          }

          if (heroVelocityMax > 8) {
            heroVelocityMax *= (1/heroSpeed);
          } else {
            heroVelocityMax = 8;
          }
        }

        if (frameCount%120 === 0) {
          if (growSpeed) {
            if (speedBarsGroup.length > 0) {
              xSpeedBarSprite = speedBarsGroup.get(speedBarsGroup.length - 1).position.x + 65;
              speedBarSprite = createSprite(xSpeedBarSprite , 820);
              speedBarSprite.addImage(logoHeroBarImg);
              speedBarsGroup.add(speedBarSprite)
            } else {
              speedBarSprite = createSprite(850 , 820);
              speedBarSprite.addImage(logoHeroBarImg);
              speedBarsGroup.add(speedBarSprite)
            }
          }
          if(speedBarsGroup.length === 3) {
            growSpeed = false;
          } else {
            if( frameCount% 80 === 0 ) {
              growSpeed = true;
            }
          }
        }

        if( frameCount%100 === 0) {
          if(speedBar < 100) {
            speedBar += 5;
          }
        }
      }
    } else {
      if(keyWentDown(CONTROL) && nbreOfBatarang > 0) {
        nbreOfBatarang--;
        let batarang = createSprite(hero.position.x, hero.position.y);
        batarang.addAnimation('normal', batarangImg);
        batarang.setSpeed(10+hero.getSpeed(), rotationBatarang);
        batarang.life = 35;
        batarangsGroup.add(batarang);
        injuredGroup.overlap(batarangsGroup, hitWithBatarang);
      }
    }

    hero.animation.frameDelay = heroSpeed;

    for(let i = 0; i < injuredGroup.length; i++) {
      if(injuredGroup[i].overlap(hero)) {
        injuredGroup.splice(i, 1);
      }
    }


    //set the camera position to the hero position
    camera.position.x = hero.position.x;
    camera.position.y = hero.position.y;

    //limit the hero movements
    if(hero.position.x < -SCENE_W)
      hero.position.x = -SCENE_W;
    if(hero.position.y < -SCENE_H)
      hero.position.y = -SCENE_H;
    if(hero.position.x > SCENE_W)
      hero.position.x = SCENE_W;
    if(hero.position.y > SCENE_H)
      hero.position.y = SCENE_H;

    if (injuredGroup.length === 0) {
      fullTime = 90;
      nbrePizzas = 50;
      gameIsOver = true;
      gameStart = false;
    }
    //draw the scene
    drawSprites(bg);
    drawSprites(injuredGroup);
    drawSprites(batarangsGroup);
    //shadow flash or batman
    noStroke();
    fill(0,0,0,20);
    //shadow
    ellipse(hero.position.x, hero.position.y+30, 30, 30);
    //character on the top
    drawSprite(hero);

    camera.off();
    fill(250, 230, 0);
    textSize(20);
    // text('FPS: ' + Math.round(frameRate()), 20, 40);
    text('Time left: ' + fullTime + ' s', 20, 40);
    text('Food left: ' + injuredGroup.length, 20, 60);
    text('Press echap to quit the game!', 20, 80);
    if (isHeroFlash) {
      drawSprites(speedBarsGroup);
    } else {
      drawSprites(nbreBatarangsGroups);
      fill(250, 230, 0);
      textSize(20);
      text(nbreOfBatarang, 820, 830);
    }
}

function hitWithBatarang(injured, bat) {
  injured.remove();
  bat.remove();
}

function endGamePage() {
  camera.off();
  background(0);
  let score = nbrePizzas - injuredGroup.length;

  if (score < 15) {
    fill(250, 230, 0);
    textSize(20);
    text('You need to improve your skills young padawan!', 320, 400);
  } else if (score < 30) {
    fill(250, 230, 0);
    textSize(20);
    text('Stay determined!', 450, 400);
  } else if (score < 40) {
    fill(250, 230, 0);
    textSize(20);
    text('Almost there !', 460, 400);
  } else if (score < 50){
    fill(250, 230, 0);
    textSize(20);
    text('That\'s actually a pretty great score ! Congratulations', 250, 400);
  } else {
    fill(250, 230, 0);
    textSize(20);
    text('THIS IS A PERFECT, YOU ARE NOW A JEDI MASTER ! CONGRATS', 250, 400);
  }
  text('Your score : ' + score, 450, 450);
  text('Press ENTER to restart the game!', 360, 500);


  if(keyWentDown(ENTER)) {
    gameIsOver = false;
    gameStart = false;
    clear();
  }
}


function chrono() {
  timer = setInterval(setTimer, 1000);
}

function setTimer() {
  if (timerLogo > 0 && !gameStart && !gameIsOver && showLogoSpyman) {
      timerLogo--;
  }

  if (timerLogo === 0) {
    showLogoSpyman = false;
  }

  if (fullTime > 0 && gameStart && !gameIsOver) {
    fullTime--;
  }

  if (fullTime === 0) {
    fullTime = 90;
    nbrePizzas = 50;
    gameIsOver = true;
    gameStart = false;
  }
}
